
# Variables

Environment variables are powerful tools to parametrize koad:io, as they 
allow to coordinate an environment for a multitude of apps to operate sanctimoniously. 
This is especially useful to include technical data from other processes and add central
variables via `.env`.

* `/home/user/.koad-io/.env`  
The first of many `.env` files that are loaded during a koad:io command.  
* `/home/user/.entity/.env`  
The second `.env` file that is loaded during a koad:io command.  The word entity is replaced by your entities name (in lower case). ie: `/home/koad/.alice/.env`  

each command will also export it's own `.env` files. 

for example
command..
```bash
alice probe electrum [server] [port] [protocol]
alice probe electrum explorercanadaecoin.ca 34333 ssl
```
the `probe electrum` is a dual, nested command.  

koad:io looks for the command `electrum` within the command `probe`'s folder,  each of these levels offer an opportunity to drop a `.env` file.  

A command can be many levels deep, consider the command `alice check email server certificates`.  The command is `certificates` inside the `server` command inside the `email` command inside the `check` command.  





These commands can be carried with you through time, and evolve into the ultimate personal productivity solution.