# Week 9; Monday March 1st 2021  
1st week of the month;
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday March 8th 2021](/minutes/2021-03-08)  
Prevous meeting [Monday Feburary 22nd 2021](/minutes/2021-02-22)  

Original record [office://X9D8xyuvMgMuzuAwrQZgrT](https://office.communitycoins.net/grain/X9D8xyuvMgMuzuAwrQZgrT/)  

## In attendance  
- Gerrit (efl)
- koad (cdn)
- Sizzlephizzle (Proxynode)
- Shenki3

## Proposed subjects  
- Community Building
	- Fujicoin [FUJI](https://ecoincore.com/network/wKL9AKpQ2giqrF92S) - Electrum only for Mainnet. 0 Online. (01/Mar/21)
	- PakCoin [PAK](https://ecoincore.com/network/WwvfcgSxv6ETqPEqf) - Koad reached out - no reply
	- Deutsche eMark [DEM](https://ecoincore.com/network/kpMAuE6NmB68rxDF5) - Shenki insulted them?
	- Auroracoin [AUR](https://ecoincore.com/network/BTbtg8S6B6wC5iRfs) - Orig dev left, new Dev not good at communicating with Icelandic leader. Michael knowledgable.
	- Europecoin [ERC](https://ecoincore.com/network/A9KyHzTEjFLgxjj6h) - Mattias is an active community member now.
- Technology
	- Use-cases vs mainstream adoption / 
	- Craig Wright's BS claims / bitcoin law
	- Atomic swaps, atomicdex.io
	- Liquidity a big blocker for corporate adoption
	- Bitcoin ATMs cost circa $8000 and provide little to no income after taxes
	- Blockchain being used independently would be good (Uploading content to the DB).
	- If a team would want to set up a communitycoin what would be our advise.


## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


