

# Week 8; Monday February 21st 2022  
3rd week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday February 28th 2022](/minutes/2022-02-28),  
Prevous meeting [Monday February 14th 2022](/minutes/2022-02-14)

## This meeting has not yet happened,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


