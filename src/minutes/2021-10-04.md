

# Week 40; Monday October 4th 2021  
1st week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday October 11th 2021](/minutes/2021-10-11),  
Prevous meeting [Monday September 27th 2021](/minutes/2021-09-27)

Original record [office://ez4exht6kk6ouRRHxoaMs4](https://office.communitycoins.net/grain/ez4exht6kk6ouRRHxoaMs4/)  

## In attendance  
- Shenki3
- koad
- Gerrit
- Mikael
- Rico
- Jeff  

# Proposed subjects
- discussion of first dev day
- expansion through bitcointalk? rico? can do a post?
	- we have a post:https://bitcointalk.org/index.php?topic=5272741.0

# Off-topic topics:
- Intro to rico: 
	- Rico is social translator for privateness:  he makes nerd talk understandable to regular people.
	- round of intros to rico:  Mikael from Auroracoin [AUR], Gerrit from Electronic Gulden [EFL],  Jason from Canada eCoin [CDN]
	- Intro to community coins -> http://communitycoins.org [Gerrit]
	- Intro to custodial awareness > https://ecoincore.com/initiatives/custodial-awareness
- Potential shared projects:
	- Diagrams that show how we are intermingling : ie, we exist on the same things (atomicdex, freiexchange, eCoinCore/Mobile-wallet) (Mikael)
	- New Coin/Project On-boarding process. (mikael)
	- New source of info (to replace coinmarketcap (and others) with a more detailed version specific to ourselves)
	- a communitycoins version of coinmaketcap. Other metrices than those CC uses that are important to us.
	- A documentation organization tool with tags/search for our completed minutes/meetings (koad)
	- Jeff says: "We are using worm."  https://github.com/alex014/worm 
	- A live document for listing each's social channels and social activity
	- Clean/Redesign communitycoins.org (rico)

- Marketing plan:
    - Posts on socials
	- bitcointalk
	- twitter
	- Videos
	- youtube
	- lbry / odysee

- Place to draft post for socials: lets explore a place to write/draft articles
	- sandstorm
	- google docs (shit)
	- git-repos

- An idea that gets excitement can go to form a team that can/will get the project started/ (shenki)
- A project owner/manager can emerge and take responsibility of a single project (Mikael)
- Each of us should share what we think is valuable with each other, and show-and-tell things that might be useful. (Gerrit)
    