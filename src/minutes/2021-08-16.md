

# Week 33; Monday August 16th 2021  
3rd week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday August 23rd 2021](/minutes/2021-08-23),  
Prevous meeting [Monday August 9th 2021](/minutes/2021-08-09)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


