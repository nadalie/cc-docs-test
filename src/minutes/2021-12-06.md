

# Week 49; Monday December 6th 2021  
1st week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday December 13th 2021](/minutes/2021-12-13),  
Prevous meeting [Monday November 29th 2021](/minutes/2021-11-29)

[office://ECrBC75j8xN68QdgJq5XEF](https://office.communitycoins.net/grain/ECrBC75j8xN68QdgJq5XEF)  


## This meeting has not yet happened,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


