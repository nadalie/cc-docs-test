

# Week 23; Monday June 7th 2021  
1st week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday June 14th 2021](/minutes/2021-06-14),  
Prevous meeting [Monday May 31st 2021](/minutes/2021-05-31)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


