

# Week 6; Monday February 7th 2022  
1st week of the month; Development meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday February 14th 2022](/minutes/2022-02-14),  
Prevous meeting [Monday January 31st 2022](/minutes/2022-01-31)

## This meeting has not yet happened,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


