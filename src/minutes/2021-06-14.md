

# Week 24; Monday June 14th 2021  
2nd week of the month; Core/Executive meeting;  
1400h EST, 1800h GMT, 2000h CET  
  
Next meeting [Monday June 21st 2021](/minutes/2021-06-21),  
Prevous meeting [Monday June 7th 2021](/minutes/2021-06-07)

## This meeting has not yet been archived,

## In attendance  
- Nothing yet  

## Proposed subjects  
- Nothing yet  

## Off-topic topics discussed  
- Nothing yet  

## Round table  
- Nothing yet  


